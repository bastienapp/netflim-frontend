import Title from "../../components/Title";

function HomePage() {
  return (
    <>
      <Title content="Netflim" />
      <p>
        Lorem ipsum dolor sit amet consectetur adipisicing elit. Magni voluptas
        rem esse possimus corrupti earum incidunt nostrum soluta praesentium
        cumque, nihil culpa at, ipsam dolorem neque sit libero voluptates
        assumenda.
      </p>
    </>
  );
}

export default HomePage;
